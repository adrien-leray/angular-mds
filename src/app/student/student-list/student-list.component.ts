import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { Student } from 'src/app/models/student';
import { StudentService } from 'src/app/services/student.service';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.scss']
})
export class StudentListComponent implements OnInit {

  students: Observable<Student[]> = null;
  formStatus: boolean = false;
  form: FormGroup;

  constructor(private studentService: StudentService, private fb: FormBuilder) { }

  ngOnInit() {
    this.students = this.studentService.list().pipe(shareReplay());

    this.form = this.fb.group({
      'name': ['', Validators.required],
      'age': ['', [Validators.required]],
      'description': ['', [Validators.required]]
    });
  }

  showHideForm() {
    this.formStatus = !this.formStatus;
  }

  major30() {
    this.students = this.studentService.getMajorThan30();
  }

  showOnlyInitials() {
    this.students = this.studentService.getStudentsInitials();
  }

  addStudent(data: any) {
    let student: Student = Object.assign(new Student(), data);
    this.studentService.addStudent(student);
    this.students = this.studentService.list().pipe(shareReplay());
  }
}
