import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { Student } from 'src/app/models/student';
import { StudentService } from 'src/app/services/student.service';

@Component({
  selector: 'app-student-detail',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.scss']
})
export class StudentDetailComponent implements OnInit {

  student: Observable<Student> = null;
  form: FormGroup;

  constructor(private studentService: StudentService, private route: ActivatedRoute, private router: Router, private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      'name': ['', Validators.required],
      'age': ['', [Validators.required]],
      'description': ['', [Validators.required]]
    });

    this.student = this.studentService.get(this.route.snapshot.paramMap.get('id')).pipe(shareReplay());

    this.student.subscribe(student => {
      this.form.setValue({
        'name': student.name,
        'age': student.age,
        'description': student.description
      });
    });
  }

  deleteItem(student: Observable<Student>) {
    // this.studentService.removeStudent();
    student.subscribe(student => { this.studentService.removeStudent(student.id); });
    this.router.navigate(['/students']);
  }

  updateStudent(data: any) {
    let student: Student = Object.assign(new Student(), data);
    this.studentService.updateStudent(student);
    this.router.navigate(['/students']);
  }
}
