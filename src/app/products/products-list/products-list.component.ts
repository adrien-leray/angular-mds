import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductsService } from 'src/app/services/products.service';
import { Supplier } from 'src/app/models/supplier';


@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {

  // Resources for data table
  displayedColumns: string[] = ['Name', 'Description', 'Ingredients', 'Price', 'Actions'];
  dataSource = [];

  listOfProducts: Map<string, Product>;

  productForm: FormGroup;

  formDisplay: boolean = true;

  constructor(private fb: FormBuilder, private productsService: ProductsService) { }

  ngOnInit() {
    this.listOfProducts = this.productsService.getProducts();
    this.dataSource = Array.from(this.listOfProducts);

    this.productForm = this.fb.group({
      'name': ['', Validators.required],
      'supplier': ['', [Validators.required]],
      'age': ['', [Validators.required]],
      'description': ['', [Validators.required]]
    })
  }

  toggleForm() {
    this.formDisplay = !this.formDisplay;
  }

  /**
   * Delete one product of the list by id
   * @param id id of the given product
   */
  deleteProduct(id: string) {
    this.productsService.deleteProduct(id);
    this.dataSource = Array.from(this.productsService.getProducts());
  }

  sendForm(data: any) {
    this.formDisplay = true;
    let product = Object.assign(new Product(), data);
    product.supplier = new Supplier(data.supplier);
    
    this.productsService.addProduct(product);
    this.dataSource = Array.from(this.productsService.getProducts());
  }

}
