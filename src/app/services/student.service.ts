import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { Student } from '../models/student';
import { NotificationService } from './notification.service';


@Injectable({
  providedIn: 'root'
})
export class StudentService {

  private students: Student[] = [];
  private students$: Observable<Student[]> = null;

  constructor(private notify: NotificationService, private http: HttpClient) { 
    this.students$ = this.http.get<Student[]>(`${environment.apiUrl}/students`)
      .pipe(tap(data => console.log('LIST ', data)));
    this.students$.subscribe(students => { this.students = students });
  }

  get(id: string): Observable<Student> {
    return this.http.get<Student>(`${environment.apiUrl}/students/${id}`)
      .pipe(tap(data => console.log('GET ID ', data)));
  }

  list(): Observable<Student[]> {
    return this.students$;
  }

  getMajorThan30(): Observable<Student[]> {
    this.students = this.students.filter(student => student.age > 30)
    this.students$ = of(this.students);
    return this.students$;
  } 

  getStudentsInitials(): Observable<Student[]> {   
    this.students = this.students.map(student => {
      let all_names = student.name.split(" ");
      student.name = all_names.map(name => name.toUpperCase().charAt(0)).join(" ");
      return student;
    });
    this.students$ = of(this.students);
    return this.students$;
  }

  addStudent(student: Student) {
    return this.http.post<Student>(`${environment.apiUrl}/students/`, new Student(null, student.name, student.age, student.description))
      .subscribe(data => { 
        this.students.push(data);
        this.students$ = of(this.students);
      });
  }

  updateStudent(student: Student) {
    console.log('TRY TO UPDATE', student);
    return this.http.put<Student>(`${environment.apiUrl}/students/`, new Student(student.id, student.name, student.age, student.description))
      .subscribe(data => {
        console.log('updated', data);
        this.students[student.id] = data;
        this.students$ = of(this.students);
      })
  }

  removeStudent(id: string): void {
    this.http.delete<Student>(`${environment.apiUrl}/students/${id}`)
      .subscribe(data => {
        console.log('DELETE', data);
      });
  }
}
