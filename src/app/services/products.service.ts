import { Injectable } from '@angular/core';
import { v4 as uuid } from 'uuid';
import { Product } from '../models/product';
import { Supplier } from '../models/supplier';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  
  products: Map<string, Product>;

  constructor() {
    this.products = DATA_PRODUCTS;
  }

  getProducts() {
    return this.products;
  }

  addProduct(product: Product) {
    this.products.set(uuid(), product);
  }

  deleteProduct(id: string) {
    this.products.delete(id);
  }
}


export const DATA_PRODUCTS = new Map()
.set(uuid(), new Product("Moutarde l'originale", new Supplier("Monoprix", "contact@monoprix.com"), ["eau", "graine de moutarde", "vinaigre"], "Moutarde de Dijon", 1, "A conserver au réfrigérateur après ouverture.", 1.16 ))
.set(uuid(), new Product("Lion wild", new Supplier("Monoprix", "contact@monoprix.com"), ["Céréales 47,3%", "maltodextrine", "caramel en poudre 2,1%"], "Céréales prêtes à consommer enrichies en : 5 vitamines, calcium, fer", 1, "Bien refermer le sachet intérieur après chaque utilisation et conserver dans un endroit sec.", 2.29 ))
.set(uuid(), new Product("Vanish oxi action poudre rose 750g format familial", new Supplier("Monoprix", "contact@monoprix.com"), [], "Vanish Oxi Action poudre Rose 750g format familial", 1, "", 6.09 ))
.set(uuid(), new Product("Bâtonnets moëlleux de surimi", new Supplier("Monoprix", "contact@monoprix.com"), ["Chair de POISSON 42 %", "eau", "amidons"], "Préparation à base de chair de poisson", 1, "", 2.85 ))
.set(uuid(), new Product("Ketchup -50% sucre", new Supplier("Monoprix", "contact@monoprix.com"), ["Tomates", "vinaigre", "sucre"], "Moutarde de Dijon", 1, "Conserver au réfrigérateur après ouverture. A consommer de préférence avant fin : voir bouchon", 1.79 ));
