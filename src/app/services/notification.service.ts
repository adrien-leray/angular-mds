import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  messages: string[] = [];
  messages$: Observable<string[]>;

  constructor(public snackBar: MatSnackBar) { 
    this.messages$ = of(this.messages);
  }

  list(): Observable<string[]> {
    return this.messages$;
  }

  add(msg: string) {
    this.messages.push(msg);
    this.messages$ = of(this.messages);
    this.snackBar.open(msg, 'Close');
  }

  clear() {
    this.messages = [];
    this.messages$ = of(this.messages);
  }
}
