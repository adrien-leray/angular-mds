import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { v4 as uuid } from 'uuid';
import { Student } from 'src/app/models/student';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  constructor() { }

  createDb() {
    const students = [
      new Student(uuid(), "adrien Leray", 18 ,"the first student"),
      new Student(uuid(), "Jean Paul Jacques", 18 ,"the second student"),
      new Student(uuid(), "Flo Flo", 19 ,"the third student"),
      new Student(uuid(), "Jean Jackie", 31 ,"student 4"),
      new Student(uuid(), "Damien Lerageu", 20 ,"student 5")
    ];

    return {students};
  }

  genId(students: Student[]) {
    return uuid();
  }
}
