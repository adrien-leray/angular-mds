import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import { NotificationComponent } from "./notification/notification.component";
import { PagenotfoundComponent } from "./pagenotfound/pagenotfound.component";
import { ProductsListComponent } from "./products/products-list/products-list.component";
import { StudentDetailComponent } from "./student/student-detail/student-detail.component";
import { StudentListComponent } from "./student/student-list/student-list.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "/home",
    pathMatch: "full"
  },
  { path: "home", component: HomeComponent, data: { title: "Home" } },
  {
    path: "students",
    component: StudentListComponent,
    data: { title: "Students list" }
  },
  {
    path: "students/:id",
    component: StudentDetailComponent,
    data: { title: "Student Details" }
  },
  { path: "products", component: ProductsListComponent, data: { title: "Products list" } },
  { path: "**", component: PagenotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
