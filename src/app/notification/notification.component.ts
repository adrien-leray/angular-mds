import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { NotificationService } from '../services/notification.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  form: FormGroup;
  formStatus: boolean = false;
  notifications: Observable<string[]> = null;

  constructor(private notify: NotificationService, private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      'message': ['', Validators.required]
    });

    this.notifications = this.notify.list();
  }

  addNotif(data: any) {
    this.notify.add(data.message);
    this.notifications = this.notify.list();
  }

  clearNotifications() {
    this.notify.clear();
    this.notifications = this.notify.list();
  }

  toggleForm() {
    this.formStatus = !this.formStatus;
  }
}
