import { refreshDescendantViews } from '@angular/core/src/render3/instructions';

export class Supplier {
    name: string;
    email: string;

    constructor(name = "", email = "") {
        this.name = name;
        this.email = email;
    }
}
