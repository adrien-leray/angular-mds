import { Supplier } from './supplier';

export class Product {
    name: string;
    supplier: Supplier;
    ingredients: string[];
    description: string;
    age: number;
    conservation: string;
    price: number;

    constructor(name = "", supplier = null, ingredients = [], description = "", age = 0, conservation = "", price = 0) {
        this.name = name;
        this.supplier = supplier;
        this.ingredients = ingredients;
        this.description = description;
        this.age = age;
        this.conservation = conservation;
        this.price = price;
    }
}