export class Student {
    id: string;
    name: string;
    age: number;
    description: string;

    constructor(id: string = null, name: string = "Anonymous", age = 0, description: string = "No description") {
        this.id = id,
        this.name = name;
        this.age = age;
        this.description= description;
    }
}
